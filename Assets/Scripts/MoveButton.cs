﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveButton : MonoBehaviour
{
    public Rigidbody rb;
    public float playerSpeed;
    public float jumpPower;
    public int directionInput;
    public bool groundCheck;
    public bool facingRight = true;


    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    void FixedUpdate()
    {
        rb.velocity = new Vector3(playerSpeed * directionInput, rb.velocity.y);
    }

    public void Move(int InputAxis)
    {

        directionInput = InputAxis;
       
    }


}

