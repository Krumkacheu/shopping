﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Move : MonoBehaviour
{
    [SerializeField] private Transform player; //ссылка на объект вокруг которого происходит движение
    [SerializeField] private float speed = 6.0f; //скорость движения
    [SerializeField] private float rotSpeed = 15.0f; // скорость поворота
    [SerializeField] private float forse = 2.0f; //сила соприкосновения
    [SerializeField] private float gravity = -9.8f; //сила гравитации
    private CharacterController  charController; 
    private ControllerColliderHit contact; //для сохранения данных о столкновнии между функиями
    private MobileController mobileControl; // ссылка на срипт MobileController
    void Start()
    {
        charController = GetComponent<CharacterController>();

        mobileControl = GameObject.FindGameObjectWithTag("Joystick").GetComponent<MobileController>();
    }

    void Update()
    {
        Vector3 movement = Vector3.zero; // обнулям вектор

        // для клавиатуры
        // float horMove = Input.GetAxis("Horizontal");
        // float verMove = Input.GetAxis("Vertical");

        //для джойстика
        float horMove = mobileControl.Horizontal() * speed; 
        float verMove = mobileControl.Vertical() * speed;

        if (horMove != 0 || verMove != 0) //обрабатываем движеие только при нажатии клавишь сто стрелками
        {
            movement.x = horMove * speed;
            movement.z = verMove * speed;
            movement = Vector3.ClampMagnitude(movement, speed); //ограничение скорости движения по диагонали

            Quaternion tmp = player.rotation; // начальная ориентация к которой перейдем после движения        
            player.eulerAngles = new Vector3(0, player.eulerAngles.y, 0);
            movement = player.TransformDirection(movement);//преобразование направление движения из локальных в глобальные координаты
            player.rotation = tmp;

            //метод LookRotation() вычисляет кватернион смотрящий в этом направлении
            Quaternion direction = Quaternion.LookRotation(movement);
            //Lerp - linear iterpolation линейная интерполяция для плавного перемещения в стороны
            transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
        }
        movement.y = gravity; //гравитация

        movement *= Time.deltaTime; //что б не зависить от FPS
        charController.Move(movement);//само пердвижение
    }
    
    //для физической силы в сцене
    private void OnControllerColliderHit(ControllerColliderHit hit) 
    {
        contact = hit;    
        //проверка, есть ли у учавствовавшего в столкновении объекта компонент Rigidbody,
        //обеспечивающий реакцию на приложенную силу
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic)
        {
            body.velocity = hit.moveDirection * forse;
        }
    }
}
