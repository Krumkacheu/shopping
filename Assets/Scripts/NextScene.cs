﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class NextScene : MonoBehaviour
{
    public void SceneOne()
    {
        SceneManager.LoadScene("1");
    }

    public void SceneTwo()
    {
        SceneManager.LoadScene("2");
    }

    public void Exit()
    {
        SceneManager.LoadScene("0");
    }

    public void Quit()
    {
        print("Вышли");
        Application.Quit();
    }
}


