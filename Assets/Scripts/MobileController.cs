﻿using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.EventSystems; //отвечает за события в игре


public class MobileController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler //интерфейсы для работы с джойстиком
{
    private Image joystickBG;
    [SerializeField] private Image joystick;
    private Vector2 inputVector; //полученные координаты джойстика

    private void Start()
    {
        joystickBG = GetComponent<Image>();
        joystick = transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector2.zero; // при отжатии джойстика обнуляем направление
        joystick.rectTransform.anchoredPosition = Vector2.zero; //возвращаем джойстик в нулевую точку
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos; //определяем позицию касания на джойстик
        //сравниваем угол отклонения с местом касания и центром
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickBG.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / joystickBG.rectTransform.sizeDelta.x);
            pos.y = (pos.y / joystickBG.rectTransform.sizeDelta.x);

            //inputVector = new Vector2(pos.x * 2 - 1, pos.y * 2 - 1); //установка точных координат из касания
            inputVector = new Vector2(pos.x, pos.y);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            //движение для джойстика изменяя позицию якоря
            joystick.rectTransform.anchoredPosition = new Vector2(inputVector.x * (joystickBG.rectTransform.sizeDelta.x / 2), inputVector.y * (joystickBG.rectTransform.sizeDelta.y / 2));
        }
    }

    public float Horizontal() //проверяем откуда с клавиатуры или джойстика для x оси
    {
        if (inputVector.x !=0) return inputVector.x;
        else return Input.GetAxis("Horizontal");
    }

    public float Vertical() //проверяем откуда с клавиатуры или джойстика для y оси
    {
        if (inputVector.y !=0) return inputVector.y;
        else return Input.GetAxis("Vertical");
    }
}
